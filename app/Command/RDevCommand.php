<?php

// Made with ❤️ By RDev Team

namespace App\Command;

use Auth;
use App\Models\Posts;

class RDevCommand {

    public static function CountPost()
    {
        return Posts::count();
    }
    
    public static function Slider($component)
    {
        $count = Posts::count();

        if ($count != 0) {
            if ($component == 'slider') {
                return Posts::orderBy('created_at', 'desc')->take(3)->get();
            } else {
                if (Posts::count() > 1) {
                    return '<div class="t-center" id="btnSlide">';
                }
            }
        }
    }

    public static function News($pagin = 0)
    {
        return Posts::orderBy('created_at', 'desc')->paginate($pagin);
    }

    public static function SideNews($slug)
    {
        return Posts::orderBy('created_at', 'desc')->where('slug', '!=', $slug)->take(2)->get();
    }
}