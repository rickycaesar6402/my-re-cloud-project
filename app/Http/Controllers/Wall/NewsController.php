<?php

namespace App\Http\Controllers\Wall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use Str;
use Storage;
use App\Models\Posts;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Apps.Public.News.Index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Apps.Public.Post.Post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'thumbnail' => 'required|image',
            'title' => 'required',
            'content' => 'required',
        ]);

        $title = $request->title;
        $content = $request->content;
        $slug = Str::slug($title, '-');
        $author = Auth::id();
        $thumbnail = $request->thumbnail;
        $thumbnail_name = '8_site'.date('Y-m-d-H-i-s').'.png';

        Posts::create([
            'thumbnail' => $thumbnail_name,
            'title' => $title,
            'content' => $content,
            'slug' => $slug,
            'author' => $author,
        ]);

        Storage::disk('storage')->putFileAs('Image', $thumbnail, $thumbnail_name);

        return redirect()->route('news.show', $slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $news = Posts::where('slug', $slug)->first();
        return view('Apps.Public.News.Detail_Post', ['news' => $news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug_old)
    {
        $this->validate($request, [
            'thumbnail' => 'image',
            'title' => 'required',
            'content' => 'required',
        ]);

        $title = $request->title;
        $content = $request->content;
        $slug = Str::slug($title, '-');
        $author = Auth::id();
        $thumbnail = $request->thumbnail;
        $thumbnail_name = '8_site'.date('Y-m-d-H-i-s').'.png';

        $News = Posts::where('slug', $slug_old);

        if ($request->hasfile('thumbnail')) {
            Storage::delete($request->thumbnail);
            Storage::disk('storage')->putFileAs('Image', $thumbnail, $thumbnail_name);
            $News->update([
                'thumbnail' => $thumbnail_name,
            ]);
        }

        $News->update([
            'title' => $title,
            'content' => $content,
            'slug' => $slug,
        ]);

        return redirect()->route('news.show', $slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        Posts::findOrFail($uuid)->delete();
        return redirect()->route('profile');
    }
}
