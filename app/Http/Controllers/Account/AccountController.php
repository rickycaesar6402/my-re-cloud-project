<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth, Storage;
use App\Models\User;

class AccountController extends Controller
{
    public function indexProfile()
    {
        return view('Apps.Public.Account.Profile');
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'profile_pic' => 'image',
            'username' => 'required',
            'email' => 'required',
        ]);
        
        $profile_pic = $request->profile_pic;
        $username = $request->username;
        $email = $request->email;
        $profile_pic_name = '8_site'.date('Y-m-d-H-i-s').'.png';

        $Profile = User::findOrFail(Auth::id());

        if ($request->hasFile('profile_pic')) {
            Storage::disk('storage')->putFileAs('Image', $profile_pic, $profile_pic_name);
            $Profile->update([
                'profile_picture' => $profile_pic_name,
            ]);
        }

        if ($Profile->email != $email) {
            $Profile->update([
                'verified_at' => null,
            ]);
        }

        $Profile->update([
            'username' => $username,
            'email' => $email,
        ]);

        return redirect()->back();
    }
}
