<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use Hash;
use Mail;
use App\Models\User;
use App\Mail\VerifyAccount;

class AuthenticationController extends Controller
{   
    public function indexLogin()
    {
        return view('Apps.Public.Account.Login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|exists:users',
            'password' => 'required',
        ]);

        $email = $request->email;
        $password = $request->password;

        $user = User::where('email', $email)->first();

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $request->session()->regenerate();
            return redirect()->route('index');
        } else {
            return redirect()->back()->with('invalidLogin', 'Something wrong here');
        }
    }

    public function indexRegister()
    {
        return view('Apps.Public.Account.Register');
    }

    public function register(Request $request)
    {
        $messages = [
            'username.required'  => 'Username is required you know',
            'email.required'     => 'Email is required you know',
            'email.unique'       => 'Email already used, <u>'.$request->email.'</u> is that you?',
            'password.required'  => 'Password is required you know',
            'password.confirmed' => 'Please confirm your Password',
        ];

        $this->validate($request, [
            'username' => 'required',
            'email'    => 'required|unique:users',
            'password' => 'required|confirmed',
        ], $messages);

        $username           = $request->username;
        $email              = $request->email;
        $password           = Hash::make($request->password);

        $user = User::create([
            'username'           => $username,
            'email'              => $email,
            'password'           => $password,
        ]);
        
        Mail::to($email)->send(new VerifyAccount($username, $email));

        Auth::login($user);
        
        return redirect()->route('verifying-account');
    }

    public function indexResetPassword()
    {
        
    }

    public function resetPassword()
    {
        
    }

    public function indexDeleteAccount()
    {
        # code...
    }

    public function deleteAccount(Request $request)
    {
        
    }
}
