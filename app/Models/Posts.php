<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use App\Traits\Uuid;

class Posts extends Model
{
    use Uuid;

    use HasFactory, Notifiable;

    protected $table = 'posts';

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    protected $keyType = 'string';

    public $timestamps = true;

    protected $fillable = [
        'thumbnail',
        'title',
        'content',
        'slug',
        'author',
    ];

    public function Author()
    {
        return $this->belongsTo('App\Models\User', 'author', 'uuid');
    }
}
