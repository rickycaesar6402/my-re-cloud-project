<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Auth;
use App\Models\User;

class VerifyAccount extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $email)
    {
        $this->username           = $username;
        $this->email              = $email;
        $token_verification = sha1(rand(00000, 99999));
        $user = User::where('email', $email)->update([
            'token_verification' => $token_verification,
        ]);
        $this->token_verification = $token_verification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))->subject("Verification account")->markdown('Apps.Mail.VerifyAccount', ['username' => $this->username, 'email' => $this->email, 'token_verification' => $this->token_verification]);
    }
}
