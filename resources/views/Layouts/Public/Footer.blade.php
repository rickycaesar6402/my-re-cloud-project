<!-- Foot -->

<div class="foot" style="background-color: rgb(97, 97, 97);">
    <div class="raw padd5">
        <div class="cal6">
            <p class="t-white t-size4 t-center">about</p>
            <div class="divider-white" style="background-color:white;"></div>
            <p class="t-white"><span class="t-size3">8-Site</span> Blog for Writer, Simpler and Easier to use. More
                Feature is <span class="t-size3">Coming Soon</span></p>
        </div>

        <div class="cal6">
            <p class="t-white t-size4 t-center">contact</p>
            <div class="divider-white" style="background-color:white;"></div>
            <ul class="foot-list">
                <li>
                    <a href="https://www.instagram.com/ricky_tiaka/" class="t-white">
                        <i class="fi-xnsuxm-instagram"></i>
                        @ricky_tiaka
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/RickyCaesarAprillaTiaka/" class="t-white">
                        <i class="fi-xnsuxm-facebook-logo"></i>
                        @ricky_tiaka
                    </a>
                </li>
                <ul>
        </div>
    </div>

    <div class="foot-down t-white">
        &copy; RDev Project 2020-2021
    </div>
</div>

<!-- Foot End -->
</body>
</html>
<!-- Friconix https://friconix.com-->
<script src="{{asset('Assets/friconix.js')}}"></script>
<script src="{{asset('Assets/rdev.min.js')}}"></script>