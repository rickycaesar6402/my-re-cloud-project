<!-- 
     Valv3.exe ~ Ricky Caesar Aprilla Tiaka
     rickycaesar6402@gmail.com
    ...- .- .-.. ...- ...-- .-.-.- . -..- . / -...- / .-. .. -.-. -.- -.-- / -.-. .- . ... .- .-. / .- .--. .-. .. .-.. .-.. .- / - .. .- -.- .- .-.-.- / .-. .. -.-. -.- -.-- -.-. .- . ... .- .-. -.... ....- ----- ..--- .--.-. --. -- .- .. .-.. .-.-.- -.-. --- --
-->
<!DOCTYPE html>
<html>

<head>
    <title>{{env('APP_NAME')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- asset -->

    <link rel="stylesheet" href="{{asset('Assets/rdev.min.css')}}">
    <link rel="stylesheet" href="asset/custom.css">

    <!-- asset end -->
</head>

<body onmousewheel="autoNav()" class="body">

    <!-- Navbar -->

    <nav id="main-navbang" class="navbang" style="background-color: gainsboro;">
        <a href="{{route('index')}}" class="navbang-title title-black marg1">
            8-Site
        </a>
        <ul class="navbang-link link-black t-size2 margauto-r">
            @if (Auth::check()&&Auth::user()->verified_at != null)
            <li>
                <a href="{{route('index')}}">
                    Home
                </a>
            </li>
            <li>
                <a href="{{route('news.index')}}">
                    News
                </a>
            </li>
            @endif
        </ul>
        <ul class="navbang-link link-black t-size2 marg1-r">
            @if (Auth::check())
            @if (Auth::user()->verified_at != null)
            <li>
                <a href="{{route('profile')}}">
                    Account
                </a>
            </li>
            @endif
            <li>
                <a onclick="return confirm('Are your really want logout?')" href="{{route('logout')}}">
                    Logout
                </a>
            </li>
            @else
            <li>
                <a href="{{route('index')}}">
                    Login
                </a>
            </li>
            <li>
                <a href="{{route('register')}}">
                    Register
                </a>
            </li>
            @endif
        </ul>

    </nav>

    <!-- Navbar end -->

    <!-- Navbar Fixed -->
    <nav id="second-navbang" class="fixed-navbang" style="background-color: gainsboro;">
        <a href="{{route('index')}}" class="navbang-title title-black marg1">
            8-Site
        </a>
        <ul class="navbang-link link-black t-size2 margauto-r">
            @if (Auth::check()&&Auth::user()->verified_at != null)
            <li>
                <a href="{{route('index')}}">
                    Home
                </a>
            </li>
            <li>
                <a href="{{route('news.index')}}">
                    News
                </a>
            </li>
            @endif
        </ul>
        <ul class="navbang-link link-black t-size2 marg1-r">
            @if (Auth::check())
            @if (Auth::user()->verified_at != null)
            <li>
                <a href="{{route('profile')}}">
                    Account
                </a>
            </li>
            @endif
            <li>
                <a onclick="return confirm('Are your really want logout?')" href="{{route('logout')}}">
                    Logout
                </a>
            </li>
            @else
            <li>
                <a href="{{route('index')}}">
                    Login
                </a>
            </li>
            <li>
                <a href="{{route('register')}}">
                    Register
                </a>
            </li>
            @endif
        </ul>
    </nav>
    <!-- Navbar Fixed end -->

    <!-- Navbar Responsive -->

    <div id="bg" onclick="closeNav()"></div>
    <button id="openNav" type="button" class="btn-respon-black" onclick="openNav()">
        <i class="fi-xnsuxm-plus-solid"></i>
    </button>

    <nav id="navbang-respon" class="navbang-respon">
        <a href="{{route('profile')}}"
            class="navbang-title-respon img-size1 img-circ img-center img-prof marg4-t marg4-b"
            style="background-image: url({{asset('Storage/Image/Ricky.png')}});">
        </a>
        <ul class="navbang-link-respon">
            @if (Auth::check()&&Auth::user()->verified_at != null)
            <li>
                <a href="{{route('index')}}">
                    Home
                </a>
            </li>
            <li>
                <a href="{{route('news.index')}}">
                    News
                </a>
            </li>
            @endif
            @if (Auth::check())
            <li>
                <a onclick="return confirm('Are your really want logout?')" href="{{route('logout')}}">
                    Logout
                </a>
            </li>
            @else
            <li>
                <a href="{{route('index')}}">
                    Login
                </a>
            </li>
            <li>
                <a href="{{route('register')}}">
                    Register
                </a>
            </li>
            @endif
        </ul>
    </nav>

    <!-- Navbar Responsive end -->