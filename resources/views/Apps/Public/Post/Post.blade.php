@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->
@if ($errors->any())
    <div class="card-alert-lava marg2-t">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
@endif
<div class="gap">
    <div class="raw">
        <div class="cal9 padd1">
            <form action="{{route('news.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="group-form padd1-b">
                    <input id="title" name="title" type="text" placeholder="Title">
                </div>
                <div class="group-form padd1-b">
                    <input id="thumbnail" name="thumbnail" type="file" placeholder="Thumbnail" accept="image/*">
                </div>
                <div id="content" class="field-white marg1 padd1">
                    Content Here
                </div>
                <button class="btn btn-grass btn-box t-size2" type="submit">Post</button>
            </form>
        </div>
    </div>
</div>
<!-- Content End -->
@endsection

@push('Javascript')
<script src="https://cdn.tiny.cloud/1/6b8uaskbm3fqridpjj1g35bn0jztbapikon6k5fqwu1861ed/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
    selector: '#content',
    inline: true,
    toolbar_mode: 'sliding',
    toolbar_location: 'bottom',
    menubar : 'false',
    toolbar: 'undo redo | formatselect | alignleft aligncenter alignright alignjustify | image link | forecolor backcolor fontsizeselect bold italic strikethrough underline hr | outdent indent table codesample emoticons',
    plugins: [
                "image autolink codesample emoticons hr help link table wordcount"
            ],
    // and here's our custom image picker
    file_picker_callback: function (cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        // Note: Now we need to register the blob in TinyMCEs image blob
                        // registry. In the next release this part hopefully won't be
                        // necessary, as we are looking to handle it internally.
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        cb(blobInfo.blobUri(), {
                            title: file.name
                        });
                    };
                    reader.readAsDataURL(file);
                };
                input.click();
            }
  });
</script>
@endpush