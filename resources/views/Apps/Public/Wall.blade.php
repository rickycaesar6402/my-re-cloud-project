@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->

<div class="gap">

    <!-- Slider -->

    <div>
        @if (RDev::CountPost() != 0)
        @foreach (RDev::Slider('slider') as $slider)
        <div class="banner banner-box banner-title-bg slider"
            style="background-image: url({{asset('Storage/Image/'.$slider->thumbnail)}})">
            <div class="raw">
                <div class="cal4 banner-title">
                    <p>{{$slider->title}}</p>
                </div>
            </div>

            <div class="raw marg1 marg4-t just-end">
                <div class="cal6 banner-content">
                    <p>{{$slider->title}}</p>
                    <div class="divider-white"></div>
                    <div class="t-left">
                        <i class="t-size1">{{$slider->created_at->diffForHumans()}} By {{$slider->Author->username}}</i>
                    </div>
                    <div class="marg2-t">
                        <a href="{{route('news.show', [$slider->slug])}}"><button class="btn btn-grass btn-box">Baca Selengkapnya</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {!!RDev::Slider('btnSlider')!!}
        @endif
    </div>

    <!-- Slider End -->

    <!-- Field -->
    <div class="field-black marg2-t marg2-b t-white padd1">
        <h1 class="t-center">
            Welcome {{Auth::user()->username}}
        </h1>

        <div class="divider-white"></div>

        <p>Hi, My name is Ricky Caesar Aprilla Tiaka, who make this Website, i am very glad if you like it, if you dont please send feedback to my email rickycaesar6402@gmail.com</p>

        <br>

        <div class="raw-rev">
            <div class="cal3 t-right padd1">
                <img src="https://friconix.com/images/friconix-overview-title.svg" class="img-right" style="width: 100%;" alt="">
                <i class="t-size1">source:
                    <a target="_blank" class="t-white"
                        href="https://friconix.com/">https://friconix.com/</a>
                </i>
            </div>

            <div class="calauto marg1-r">
                <p>I build this blog with Laravel 8, CSS, Javascript and <a href="https://friconix.com/" class="t-white">Friconix</a>. Friconix is a collection of free vector icons designed to be easily embed on web pages.</p>
            </div>

        </div>
    </div>
</div>
</div>

<!-- Field End -->



<!-- Content End -->
@endsection

@push('Javascript')

@endpush