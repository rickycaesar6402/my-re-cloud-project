@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->

<div class="gap">
    <div class="raw">
        <div class="cal9 padd1">
            <div id="contentField">
                <div class="group-form padd1-b">
                    <h1>{{$news->title}}</h1>
                </div>
                <div class="divider-black"></div>
                <img class="img-roun img-center" style="width: 60%;" src="{{asset('Storage/Image/'.$news->thumbnail)}}" alt="">
                <div class="field-white marg1 padd1">
                    {!!$news->content!!}
                </div>
            </div>
            <form id="editform" action="{{route('news.update', ['news'=>$news->slug])}}" method="POST" enctype="multipart/form-data"
                style="display: none;">
                @csrf
                @method('PUT')
                <div class="group-form padd1-b">
                    <input id="title" name="title" type="text" placeholder="Title" value="{{$news->title}}">
                </div>
                <div class="group-form padd1-b">
                    <input id="thumbnail" name="thumbnail" type="file" placeholder="Thumbnail" accept="image/*">
                </div>
                <div id="content" class="field-white marg1 padd1">
                    {!!$news->content!!}
                </div>
                <button class="btn btn-grass btn-box t-size2" type="submit">Post</button>
                <button class="btn btn-lava btn-box t-size2" type="button" onclick="CloseEditForm()">Close</button>
            </form>
            <div class="raw">
                <div class="cal12 t-right">
                    <i>{{$news->created_at->diffForHumans()}} By {{$news->Author->username}}</i>
                </div>
            </div>
            @if ($news->author == Auth::id())
            <button id="edit" class="btn btn-grass btn-box t-size2" type="button" onclick="EditForm()">Edit</button>
            @endif
        </div>
        <div class="calauto raw padd1-t padd1-b">
            <div class="cal12">
                <div class="card-box">
                    <div class="card-box-body-black no-img">
                        <p class="t-size4 t-center t-white">News</p>
                    </div>
                </div>
            </div>
            @foreach (RDev::SideNews($news->slug) as $sidenews)
            <div class="cal12">
                <div class="card-box">
                    <a href="">
                        <div class="card-image" style="background-image: url({{asset('Storage/Image/'.$sidenews->thumbnail)}});"></div>
                    </a>
                    <div class="card-box-body-white with-img">
                        <div class="card-title">
                            <a href="{{route('news.show', [$sidenews->slug])}}" class="t-size3 t-black">{{$sidenews->title}}</a>
                        </div>
                        <p class="t-black t-center">
                            {!!$sidenews->content!!}
                        </p>
                        <div class="raw t-size1">
                            <div class="cal5 t-left">
                                <p class="t-black">{{$sidenews->created_at->diffForHumans()}} by {{$sidenews->Author->username}}</p>
                            </div>
                            <div class="cal7 t-right">
                                <a href="{{route('news.show', [$sidenews->slug])}}" class="t-black">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            @endforeach
        </div>
    </div>
</div>
<!-- Content End -->
@endsection

@push('Javascript')
<script src="https://cdn.tiny.cloud/1/6b8uaskbm3fqridpjj1g35bn0jztbapikon6k5fqwu1861ed/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<script>
    function EditForm() {
        document.getElementById("contentField").style.display = "none"
        document.getElementById("editform").style.display = "block"
        document.getElementById("edit").style.display = "none"
    }

    function CloseEditForm() {
        document.getElementById("contentField").style.display = "block"
        document.getElementById("editform").style.display = "none"
        document.getElementById("edit").style.display = "block"
    }
</script>
<script>
    tinymce.init({
        selector: '#content',
        inline: true,
        toolbar_mode: 'sliding',
        toolbar_location: 'bottom',
        menubar: 'false',
        toolbar: 'undo redo | formatselect | alignleft aligncenter alignright alignjustify | image link | forecolor backcolor fontsizeselect bold italic strikethrough underline hr | outdent indent table codesample emoticons',
        plugins: [
            "image autolink codesample emoticons hr help link table wordcount"
        ],
        // and here's our custom image picker
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), {
                        title: file.name
                    });
                };
                reader.readAsDataURL(file);
            };
            input.click();
        }
    });
</script>
@endpush