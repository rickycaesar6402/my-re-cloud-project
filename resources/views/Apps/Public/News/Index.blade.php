@extends('Layouts.Public.Master')
@section('Content')
<!-- Content -->

<div class="gap">
    <!-- Slider -->

    <div>
        @if (RDev::CountPost() != 0)
        @foreach (RDev::Slider('slider') as $slider)
        <div class="banner banner-box banner-title-bg slider"
            style="background-image: url({{asset('Storage/Image/'.$slider->thumbnail)}})">
            <div class="raw">
                <div class="cal4 banner-title">
                    <p>{{$slider->title}}</p>
                </div>
            </div>

            <div class="raw marg1 marg4-t just-end">
                <div class="cal6 banner-content">
                    <p>{{$slider->title}}</p>
                    <div class="divider-white"></div>
                    <div class="t-left">
                        <i class="t-size1">{{$slider->created_at->diffForHumans()}} By {{$slider->Author->username}}</i>
                    </div>
                    <div class="marg2-t">
                        <a href="{{route('news.show', [$slider->slug])}}"><button class="btn btn-grass btn-box">Baca Selengkapnya</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {!!RDev::Slider('btnSlider')!!}
        @endif
    </div>


    <!-- Slider End -->
    <div class="marg2-t marg2-b">
        <div class="raw">
            @foreach (RDev::News(8) as $news)
            <div class="cal3">
                <div class="card-box">
                    <a href="{{route('news.show', [$news->slug])}}">
                        <div class="card-image"
                            style="background-image: url({{asset('Storage/Image/'.$news->thumbnail)}});"></div>
                    </a>
                    <div class="card-box-body-white with-img">
                        <div class="card-title">
                            <a href="{{route('news.show', [$news->slug])}}" class="t-size3 t-black">{{$news->title}}</a>
                        </div>
                        <p class="t-black" href="">
                            {{$news->title}}
                        </p>
                        <div class="raw t-size1">
                            <div class="cal5 t-left">
                                <p class="t-black">{{$news->created_at->diffForHumans()}}</a>
                            </div>
                            <div class="cal7 t-right">
                                <a href="{{route('news.show', [$news->slug])}}" class="t-black">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

{{RDev::News(8)->links('vendor.pagination.bootstrap-4')}}
</div>

<!-- Content End -->
@endsection

@push('Javascript')

@endpush