@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->

<div class="ungap">
    <div class="raw">
        <div class="cal2">
            <div class="sidebang">
                <ul>
                    <li>
                        <a class="side-i" href="#1"><i class="fi-xnsrxm-chevron-solid"></i> Account</a>
                    </li>
                    <li>
                        <a class="side-i" href="{{route('news.create')}}"><i class="fi-xnsrxm-chevron-solid"></i>
                            Post</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="calauto padd1">
            <!-- Post List -->
            <h1>Post</h1>
            <div class="divider-black"></div>
            <div class="raw">
                @if (RDev::CountPost() != 0)
                @foreach (RDev::News(5)->where('author', Auth::id()) as $news)
                <div class="cal12">
                    <div class="card-box">
                        <div class="card-box-body-white no-img">
                            <div class="card-title">
                                <div class="raw">
                                    <div class="calauto">
                                        <a href="{{route('news.show', [$news->slug])}}"
                                            class="t-size3 t-black">{{$news->title}}</a>
                                    </div>
                                    <div class="cal1 t-right">
                                        <form action="{{route('news.destroy', ['news'=>$news->uuid])}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a href="javascript:void(0)" class="t-black"><i class="fi-xnsuxl-trash-bin"
                                                    onclick="this.closest('form').submit()"></i></a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <p class="t-black t-center">
                                {!!$news->content!!}
                            </p>
                            <div class="raw t-size1">
                                <div class="cal5 t-left">
                                    <p class="t-black">1 Days ago</p>
                                </div>
                                <div class="cal7 t-right">
                                    <a href="{{route('news.show', [$news->slug])}}" class="t-black">Baca
                                        Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            {{RDev::News(5)->links('vendor.pagination.bootstrap-4')}}
            <!-- Post List End -->
            <!-- Account Detail -->
            <h1>Account</h1>
            <div class="divider-black"></div>
            <div id="dataprofile">
                <div class="raw">
                    <div class="cal12">
                        <div class="group-form">
                            <label>Profile Picture :</label>
                            <div class="img-size3 img-circ img-center img-prof"
                                style="background-image: url({{asset('Storage/Image/'.Auth::user()->profile_picture)}});">
                            </div>
                            <!-- <img src="asset/ricky.jpg" class="img-size1 img-roun margauto-r margauto-l"> -->
                        </div>
                    </div>
                    <div class="cal6">
                        <div class="group-form">
                            <label>Username :</label>
                            <p>{{Auth::user()->username}}</p>
                        </div>
                    </div>
                    <div class="cal6">
                        <div class="group-form">
                            <label>Email :</label>
                            <p>{{Auth::user()->email}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <form id="formedit" action="{{route('update.profile')}}" method="POST" enctype="multipart/form-data" style="display: none;">
                @csrf
                @method('PUT')
                <div class="raw">
                    <div class="cal12">
                        <div class="group-form">
                            <label>Profile Picture :</label>
                            <input id="profile_pic" name="profile_pic" type="file" placeholder="Profile Pic" accept="image/*">
                        </div>
                    </div>
                    <div class="cal6">
                        <div class="group-form">
                            <label>Username :</label>
                            <input id="username" name="username" type="text" placeholder="Username"
                                value="{{Auth::user()->username}}">
                        </div>
                    </div>
                    <div class="cal6">
                        <div class="group-form">
                            <label>Email :</label>
                            <input id="email" name="email" type="email" placeholder="Email"
                                value="{{Auth::user()->email}}">
                        </div>
                    </div>
                    <div class="cal12">
                        <button type="submit" class="btn btn-water btn-box t-size2">Post</button>
                        <button type="button" class="btn btn-lava btn-box t-size2"
                            onclick="closeEditForm()">Close</button>
                    </div>
                </div>
            </form>
            <div class="padd1">
                <button id="edit" type="button" class="btn btn-water btn-box t-size2" onclick="editForm()">Edit</button>
            </div>
            <!-- Account Detail end -->

        </div>
    </div>
</div>

<!-- Content End -->
@endsection

@push('Javascript')

<script>
    function editForm() {
        document.getElementById("formedit").style.display = "block"
        document.getElementById("dataprofile").style.display = "none"
        document.getElementById("edit").style.display = "none"
    }

    function closeEditForm() {
        document.getElementById("formedit").style.display = "none"
        document.getElementById("dataprofile").style.display = "block"
        document.getElementById("edit").style.display = "block"
    }
</script>

@endpush