@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->

<div class="ungap marg5 padd5">

    {{-- Alert Card --}}

    <div class="card-alert-water">
        <p>
            Thanks for register on our blog, before that you need to check your email to verify your account
        </p>
        <a href="{{route('send.verify')}}">Send new verification mail</a>
    </div>

    {{-- Alert Card End --}}

</div>

<!-- Content End -->
@endsection

@push('Javascript')

@endpush