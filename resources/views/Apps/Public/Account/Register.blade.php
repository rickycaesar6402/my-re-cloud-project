@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->

<div class="ungap marg5 padd5">
    <!-- Register -->
    <h1>Register</h1>
    <div class="divider-black"></div>
    <form method="POST" action="{{route('register.store')}}">
        @csrf
        <div class="raw">
            @if ($errors->any())
            <div class="cal12 padd2">
                <div class="card-alert-lava">
                    <ul class="padd2-l">
                        @foreach ($errors->all() as $error)
                        <li>{!!$error!!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            <div class="cal6">
                <div class="group-form">
                    <label for="username">Username <span class="req">*</span></label>
                    <input id="username" name="username" @error('username') class="lava-form-border" @enderror
                        type="text" placeholder="Username" value="{{old('username')}}">
                </div>
            </div>
            <div class="cal6">
                <div class="group-form">
                    <label for="email">Email <span class="req">*</span></label>
                    <input id="email" name="email" @error('email') class="lava-form-border" @enderror type="email"
                        placeholder="Email" value="{{old('email')}}">
                </div>
            </div>
            <div class="cal6">
                <div class="group-form">
                    <label for="password">Password <span class="req">*</span></label>
                    <input id="password" name="password" @error('password') class="lava-form-border" @enderror
                        type="password" placeholder="Password">
                </div>
            </div>
            <div class="cal6">
                <div class="group-form">
                    <label for="re-password">Re-Password <span class="req">*</span></label>
                    <input id="re-password" name="password_confirmation" @error('password') class="lava-form-border"
                        @enderror type="password" placeholder="Re-Password">
                </div>
            </div>
        </div>
        <div class="padd1">
            <button type="submit" class="btn btn-grass btn-box t-size2">Register</button>
        </div>
    </form>
    <!-- Register End -->
</div>

<!-- Content End -->
@endsection

@push('Javascript')

@endpush