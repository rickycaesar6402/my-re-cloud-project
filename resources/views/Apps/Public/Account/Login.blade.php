@extends('Layouts.Public.Master')

@section('Content')
<!-- Content -->
<div class="ungap marg5 padd5">

    <!-- Login -->
    @if ($errors->any())
    <div class="card-alert-lava marg2-t">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
    @elseif (session('invalidLogin'))
    <div class="card-alert-lava marg2-t">
        <li>{{ session('invalidLogin') }}</li>
    </div>
    @endif
    <h1>Login</h1>
    <div class="divider-black"></div>
    <form method="POST" action="{{route('login.store')}}">
        @csrf
        <div class="raw">
            <div class="cal12">
                <div class="group-form">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="email" placeholder="Email">
                </div>
            </div>
            <div class="cal12">
                <div class="group-form">
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password" placeholder="Password">
                </div>
            </div>
        </div>
        <div class="padd1">
            <button type="submit" class="btn btn-grass btn-box t-size2">Login</button>
        </div>
    </form>

    <!-- Login End -->
</div>

<!-- Content End -->
@endsection

@push('Javascript')

@endpush