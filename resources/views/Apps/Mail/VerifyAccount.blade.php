<div class="gap">
    @php
        $color = ['grass', 'lava', 'water', 'sun'];
    @endphp
    <div class="field-{{$color[rand(0, 3)]}} marg2-t marg2-b t-black padd1">
        <h1 class="t-center">
            Welcome, {{$username}}
        </h1>

        <div class="divider-black"></div>
        
        <p>Thanks to register on our Blog, this Blog need your feedback, i am glad if you give us feedback, you can send
            your feedback on this Email</p>
        <p>This is verification link <a href="{{env('APP_URL')}}token={{$token_verification}}">{{env('APP_URL')}}token={{$token_verification}}</a></p>
    </div>
</div>