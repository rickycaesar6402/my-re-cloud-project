<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Wall\WallController as Wall;
use App\Http\Controllers\Wall\NewsController as News;
use App\Http\Controllers\Account\AuthenticationController as Authentication;
use App\Http\Controllers\Account\AccountController as Account;
use App\Mail\VerifyAccount;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'verifyingAccount'], function() {

    Route::resource('/', Wall::class);

    Route::resource('/news', News::class);

    Route::get('/profile', [Account::class, 'indexProfile'])->name('profile');

    Route::put('/profile-edit', [Account::class, 'updateProfile'])->name('update.profile');

});

Route::group(['prefix' => '/register', 'middleware' => 'guest'], function() {

    Route::get('/', [Authentication::class, 'indexRegister'])->name('register');

    Route::post('/', [Authentication::class, 'register'])->name('register.store');

});

Route::group(['prefix' => '/login', 'middleware' => 'guest'], function() {

    Route::get('/', [Authentication::class, 'indexLogin'])->name('login');

    Route::post('/', [Authentication::class, 'login'])->name('login.store');

});

Route::group(['middleware' => 'auth'], function() {
    
    Route::get('/verifying-account', function() {
        if (Auth::user()->verified_at == null) {
            return view('Apps.Public.Account.VerifyingAccount');
        } else {
            return redirect()->route('index');
        }
        
    })->name('verifying-account');

    Route::get('/send-verify', function() {
        $email    = Auth::user()->email;
        $username = Auth::user()->username;
        Mail::to($email)->send(new VerifyAccount($username, $email));
        return redirect()->route('verifying-account');
    })->name('send.verify');

    Route::get('/token={token}', function($token) {
        if (Auth::user()->token_verification == $token) {
            User::find(Auth::id())->update([
                'verified_at' => date("Y-m-d H:i:s"),
            ]);
            return redirect()->route('index');
        }
    });

    Route::get('/logout', function() {
        Auth::logout();
        return redirect()->route('index');
    })->name('logout');

});